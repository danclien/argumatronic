---
title: About
---

I was a happy, stay-at-home mom living in a tiny oilfield town in Wyoming when a secret cabal (huehue) of Haskellers kidnapped me and taught me their language. I no longer understand any other languages.

I have been a teacher, librarian, linguist, student of philosophy, and writer. I am still at least three of those things. Now I'm writing [Haskell Programming from First Principles](http://haskellbook.com/).

I'm about half hippie chick and about half redneck, sympathetic to various political and economic positions along the anti-statist and voluntarist lines. I like loud music, kung fu movies, and hard liquor. I also like arts, crafts, stories, and the people who make them. I like gardening, canning, talking to my kids, and being in the mountains. I'm interested in helping people learn to create technology, rather than simply consume it. I like strong opinions well defended. I don't like racists, misogynists, fascists, or people who buy their pie crust at the grocery store.

One of my boyfriends broke up with me because I never take life seriously enough. 

But I live by these words: *"Don't take life so serious, son, it ain't nohow permanent."*

I am pretty serious about pie crust, though. Don't even come at me with that store bought stuff.

